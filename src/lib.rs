
//! 
//! # Zvezda
//! 
//! Zvezda is a fast and lightweight web _library_, designed to provide
//! HTML capability to your website. It is **not** like [Warp][1] or [Rocket][2],
//! which are designed to provide a framework to work off of, but instead to add
//! a non-intrusive HTML backend implementation to your project.
//! 
//! ---
//! ## Hello World Example
//! 
//! ```
//! use std::net::TcpListener;
//! use std::io::Read;
//! use zvezda::Connection;
//! 
//! fn main() {
//! 
//! 
//!     let listener = TcpListener::bind("127.0.0.1:8080").unwrap();
//! 
//! 
//!     for stream in listener.incoming() {
//!         let mut stream = stream.unwrap();
//!         let mut buf = [0; 1536];
//! 
//!         stream.read(&mut buf).unwrap();
//! 
//!         let packet = String::from_utf8_lossy(&buf);
//! 
//! 
//!         let mut handle = Connection::parse(packet.to_string(), stream);
//! 
//!         let path = handle.path_as_str();
//!         match path {
//!             "/" => {
//!                 handle.write_html("200 OK", String::from("Hello World!")).unwrap();
//!             },
//!             _ => {
//!                 handle.write_res("404 NOT FOUND").unwrap();
//!             }
//!         }
//!     }
//! }
//! ```
//! 
//! 
//! [1]: https://crates.io/crates/warp
//! [2]: https://crates.io/crates/rocket
//! 


use std::collections::HashMap;
use std::io::Write;
use std::net::TcpStream;
use std::fmt::Result;


/// The enumerator used to describe the HTTP request type
#[derive(Debug)]
pub enum ReqType {
    Get,
    Post,
    Connect,
    Delete,
    Head,
    Options,
    Put
}

/// HTTP connection handler
pub struct Connection {  // Why did I make the names so confusing ;w;
                         // maybe I'll change that later
    path: Box<String>,
    pub reqtype: ReqType,
    pub arguments: Option<HashMap<String, String>>, // Path-encoded queries
    pub headers: HashMap<String, String>,
    pub useragent: String,
    pub packet: Box<String>, // The whole, unparsed client request
    pub tcpstream: TcpStream
}

/// HTTP response builder
#[derive(Debug)]
pub struct Response {
    pub response: String,
    pub headers: HashMap<String, String>,
    pub content: String

}


impl Connection {
    /// A simple builder function that constructs a `Connection`. This is not the recommended way to make a `Connection`, please see `Connection::parse()`
    pub fn new(path: String, 
        reqtype: ReqType, 
        arguments: Option<HashMap<String, String>>, 
        ua: String, 
        packet: String, 
        stream: TcpStream,
        headers: HashMap<String, String>,
    ) -> Connection 
    {


        Connection {
            path: Box::new(path),
            reqtype: reqtype,
            arguments,
            headers,
            useragent: ua,
            packet: Box::new(packet),
            tcpstream: stream
        }
    }

    /// The recommended way to construct a `Connection`. Takes the full HTTP request, as a String, parses it,
    /// and returns a `Connection`. 
    pub fn parse(packet: String, stream: TcpStream) -> Connection {
        let mut arguments = packet.split("\r\n");
        let headers = Connection::parse_headers(&packet);
        let mut head = arguments.next().unwrap().split(" ");
        let reqtype: ReqType = match head.next().unwrap().to_uppercase().as_str() {
            "GET" => {
                ReqType::Get
            },
            "POST" => {
                ReqType::Post
            },
            "CONNECT" => {
                ReqType::Connect
            },
            "DELETE" => {
                ReqType::Head
            },
            "HEAD" => {
                ReqType::Head
            },
            "OPTIONS" => {
                ReqType::Options
            },
            "PUT" => {
                ReqType::Put
            }
            _ => {
                // Its probably safe enough to handle these requests as GET requests, but if you want to sacrifice reliability for accuracy you can make it panic here instead
                // PANICING IS NOT RECOMMENDED AND ESPECIALLY DISCOURAGED ON SINGLE-THREADED BACKENDS!

                // That being said, uncomment the following line if you _do_ want panicing
                //panic!("Bad request type!");

                ReqType::Get
            }
        };
        let mut fullpath = head.next().unwrap().split("?");
        let path = fullpath.next().unwrap();

        let queries: Option<HashMap<String, String>> = match fullpath.next() {
            Some(argument) => {
                let arguments = argument.split("&");
                let mut argshashmap = HashMap::new();
                for query in arguments {
                    let mut query = query.split("=");

                    argshashmap.insert(query.nth(0).unwrap().to_string(), query.next().unwrap().to_string());
                }
                
                Some(argshashmap)
            },
            None => {
                None
            }
        };

        Connection {
            path: Box::new(path.to_string()),
            reqtype,
            arguments: queries,
            useragent: headers.get("User-Agent").unwrap().to_string(), // Probably should add error handling in the future
            headers: headers,
            packet: Box::new(packet.to_string()),
            tcpstream: stream
        }
    }

    fn parse_headers(request: &String) -> HashMap<String, String> {
        let mut headers_as_iter = request.split("\r\n");
        headers_as_iter.next();
        let mut headers: HashMap<String, String> = HashMap::new();

        for head in headers_as_iter {
            if head == "".to_string() { // Stop parsing once the header of the packet is finished (defined by two CR-LF)
                break
            }
            let mut head_kvpair = head.splitn(2, ":");
            headers.insert(head_kvpair.next().unwrap().to_string(), head_kvpair.next().unwrap().to_string());

        }

        return headers
    }

    pub fn path_as_str(&self) -> &str {
        self.path.as_str()
    }


    pub fn write_raw(&mut self, out: String) -> Result {
        self.tcpstream.write(out.as_bytes()).unwrap();
        
        self.tcpstream.flush().unwrap();

        Ok(())
    }
    /// Write an empty HTML response, where response is a raw HTML status (eg. `200 OK`, `404 NOT FOUND`, `418`)
    pub fn write_res(&mut self, response: &str) -> Result {
        self.tcpstream.write(format!("HTTP/1.1 {}", response).as_bytes()).unwrap();

        Ok(())
    }
    /// Write an HTML response, where response is a status (refer to `write_res()`), as well as the HTML contents to the body of the request. 
    /// *This function is considered depricated in light of `Response::html()`, please use that instead!*
    pub fn write_html(&mut self, response: &str, html: String) -> Result {
        let out = format!("HTTP/1.1 {}\r\ncontent-type: text/html\r\n\r\n{}", response, html);

        self.tcpstream.write(out.as_bytes()).unwrap();
        Ok(())
    }
    /// Write an HTML response, where response is a status (refer to `write_res()`), as well as the given JSON to the request body
    pub fn write_json(&mut self, response: &str, json: String) -> Result {
        let out = format!("HTTP/1.1 {}\r\ncontent-type: application/json\r\n\r\n{}", response, json);

        self.tcpstream.write(out.as_bytes()).unwrap();
        Ok(())
    }
    /// Write a `Response` constructor
    pub fn write_response(&mut self, response: Response) -> Result {
        let mut response_data = String::new();

        response_data.push_str(format!("HTTP/1.1 {}\r\n", response.response).as_str());

        for (k, v) in response.headers {
            response_data.push_str(format!("{}: {}\r\n", k, v).as_str());
        }
        response_data.push_str("\r\n");

        response_data.push_str(response.content.as_str());

        self.write_raw(response_data)
    }
}

impl Response {

    /// The standard builder function for the `Response` struct. It has the most verbosity.
    /// If you don't need the verbosity provided (ex. 200 OK response, JSON response), check
    /// the other helper functions of this struct for convenience's sake.
    pub fn new(response: &str, mimetype: &str, mut headers: HashMap<String, String>, content: String) -> Response {
        let response = String::from(response);
        let mime = String::from(mimetype);

        headers.insert(String::from("Content-Type"), mime);

        Response {
            response,
            headers,
            content
        }
    }
    /// Builds a `200 OK` HTML Response. More headers can be added in the second field, but
    /// they have to be wrapped in a `Some()` enum. Otherwise, you can use the `None` enum
    /// and no extra headers will be added onto the response.
    pub fn html_ok(html: String, headermap: Option<HashMap<String, String>>) -> Response {
        match headermap {
            Some(mut headers) => {
                headers.insert("Content-Type".to_string(), "text/html".to_string());
                Response {
                    response: "200 OK".to_string(),
                    headers,
                    content: html
                }
            }
            None => {
                let mut headers = HashMap::new();
                headers.insert("Content-Type".to_string(), "text/html".to_string());
                Response {
                    response: "200 OK".to_string(),
                    headers,
                    content: html
                }
            }
        }
    }
}
