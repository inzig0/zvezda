# Zvezda
## A web library, not a web framework

### Why?

One of my big gripes with a lot of these existing web server implementations for rust
is that they are all **big!** You need to look through documentation for 15 minutes
and 100 lines of boilerplate to even know what your doing! Thats why I began work on
Zvezda.

---

### Running included samples

1. Clone this repository: `https://gitlab.com/inzig0/zvezda.git`
2. `cd` into the project folder
3. Run one of the examples:
	- Barebones web server: `cargo run --example hello`
	- Basic ping-pong web server: `cargo run --example echo`
	- Simple boilerplate web server: `cargo run --example html`
	- JSON usage example: `cargo run --example json`


### Including this library in your project

Add this line to your `Cargo.toml` :
```
...
[dependencies]
zvezda = "0.1.5"
// OR if you want the git version,
zvezda = { git = "https://gitlab.com/inzig0/zvezda.git" }
...
```