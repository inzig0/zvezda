/*

	A complete, multi-threaded HTML webserver!

	This is great boilerplate code, its simple, yet
    still puts everything you need into a nice little
    package :3

    This code has been updated for the new Response feature, specifically Response::html_ok()

    (Given that it *is* supposed to be boilerplate, I won't bombard you with code comments, either)

*/


use std::net::TcpListener;
use std::io::Read;
use std::thread;
use std::fs;
use zvezda::Connection;
use zvezda::Response;

fn main() {

	
    let listener = TcpListener::bind("127.0.0.1:8080").unwrap();


    for stream in listener.incoming() {

        thread::spawn(move || {

            let mut stream = stream.unwrap();
            let mut buf = [0; 1536];

            stream.read(&mut buf).unwrap();

            let packet = String::from_utf8_lossy(&buf);

        
            let mut handle = Connection::parse(packet.to_string(), stream);

            let path = handle.path_as_str();
            match path {
                "/" => {
                    let html = fs::read_to_string("examples/www/html/index.html").unwrap();
                    handle.write_response(Response::html_ok(html, None)).unwrap();
                },
                _ => {
                    handle.write_res("404 NOT FOUND").unwrap();
                }
            }
        });
    }
}