/*

	An extremely simple, single-threaded web server

	This example will have a lot of explanation comments,
	I'd recommend using the html example if you need
	boilerplate

*/


use std::net::TcpListener;
use std::io::Read;

// This struct makes up most of the web server
use zvezda::Connection;

fn main() {

	// Most of this is ripped from chapter 20 of The Rust Book
    let listener = TcpListener::bind("127.0.0.1:8080").unwrap();


    for stream in listener.incoming() {
        let mut stream = stream.unwrap();
        // 1536 is an entirely arbitrary size for the buffer, you can shrink it down but chromium-based
        // browsers refuse to work properly if you dont read the entire request, so beware!
        let mut buf = [0; 1536];

        stream.read(&mut buf).unwrap();

        let packet = String::from_utf8_lossy(&buf);

        // This is when it [stuff] gets exciting! Zvezda will parse the request for you,
        // and then extract important values that it handles!
        // If for some reason you wanted to parse the request yourself you may use the new() function, but...
        // why?
        
        // MUT IS IMPORTANT! If the connection isn't mutable, you wont be able to respond to requests (big problem)
        let mut handle = Connection::parse(packet.to_string(), stream);

        let path = handle.path_as_str();
        match path {
            "/" => {
            	// You will see nothing, you should see nothing if this works. Check out the html example
            	// if you want something that actually makes a website!
                handle.write_res("200 OK").unwrap();
            },
            _ => {
                handle.write_res("404 NOT FOUND").unwrap();
            }
        }
    }
}
