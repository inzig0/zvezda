/*

	A multi-threaded webserver with JSON support

	This is meant to show off the new JSON support bundled into Zvezda!
    The name of the game is pretty much the same, just with some different functions.

*/


use std::net::TcpListener;
use std::io::Read;
use std::thread;
use std::fs;
use zvezda::Connection;

fn main() {

	
    let listener = TcpListener::bind("127.0.0.1:8080").unwrap();


    for stream in listener.incoming() {

        thread::spawn(move || {

            let mut stream = stream.unwrap();
            let mut buf = [0; 1536];

            stream.read(&mut buf).unwrap();

            let packet = String::from_utf8_lossy(&buf);

        
            let mut handle = Connection::parse(packet.to_string(), stream);

            let path = handle.path_as_str();
            match path {
                "/" => {
                    handle.write_html("200 OK", fs::read_to_string("examples/www/json/index.html").unwrap()).unwrap();
                },
                "/data" => {
                    handle.write_json("200 OK", fs::read_to_string("examples/www/json/database.json").unwrap()).unwrap();
                }
                _ => {
                    handle.write_res("404 NOT FOUND").unwrap();
                }
            }
        });
    }
}