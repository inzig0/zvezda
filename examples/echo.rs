/*

    The Alps in a program!

    This single-threaded ping-pong 
    example is made to demonstrate the
    query-handling capability of zvezda.

*/


use std::net::TcpListener;
use std::io::Read;
use std::fs;
use zvezda::Connection;

fn main() {


    let listener = TcpListener::bind("127.0.0.1:8080").unwrap();


    for stream in listener.incoming() {
        let mut stream = stream.unwrap();
        let mut buf = [0; 1536];

        stream.read(&mut buf).unwrap();

        let packet = String::from_utf8_lossy(&buf);


        let mut handle = Connection::parse(packet.to_string(), stream);

        let path = handle.path_as_str();
        match path {
            "/" => {
                handle.write_html("200 OK", fs::read_to_string("examples/www/echo/index.html").unwrap()).unwrap();
            },
            "/echo" => { // Queries are separated from paths, but the HashMap that stores queries is
                         // wrapped in an Option(), so you can see if queries were provided by
                         // testing for Some(HashMap) or None

                // Did I mention queries are stored in a HashMap?
                let val = handle.arguments.clone().unwrap();
                handle.write_html("200 OK", format!("<html><head><title>Zvezda Echo</title></head><body><p>Echo! {:?} recieved</p></body></html>", val.get("val"))).unwrap();
            }
            _ => {
                handle.write_res("404 NOT FOUND").unwrap();
            }
        }
    }
}
